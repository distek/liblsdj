package liblsdj

const TableLen = 16

type Table struct {
	Volumes        [TableLen]byte
	Transpositions [TableLen]byte
	Commands1      [TableLen]Command
	Commands2      [TableLen]Command
}

func (t *Table) Clear() {
	for i := 0; i < TableLen; i++ {
		t.Volumes[i] = 0
		t.Transpositions[i] = 0
		t.Commands1[i].Clear()
		t.Commands2[i].Clear()
	}
}

func (t *Table) GetCommand1() (out []byte) {
	for i := 0; i < TableLen; i++ {
		out = append(out, t.Commands1[i].Get()...)
	}
	return out
}

func (t *Table) GetCommand2() (out []byte) {
	for i := 0; i < TableLen; i++ {
		out = append(out, t.Commands2[i].Get()...)
	}
	return out
}
