package liblsdj

// TODO: controlla bitwise
// TODO: controlla condizione effettivamente
func parseLength(b byte) byte {
	if (b & 0x40) != 0 {
		return (^b) & 0x3F
	}
	return InstrumentUnlimitedLength
}

func parseTable(b byte) byte {
	if (b & 0x20) != 0 {
		return b & 0x1F
	}

	return NoTable
}

func parsePanning(b byte) Panning {
	return (Panning)(b & 3)
}

func parseDrumMode(b byte, version byte) byte {
	if version < 3 || (b&0x40) != 0 {
		return 0
	}
	return 1
}

func parseTranspose(b byte, version byte) byte {
	if version < 3 || (b&0x20) != 0 {
		return 0
	}
	return 1
}

func parseAutomate(b byte) byte {
	return (b >> 3) & 0x1
}

func parsePulseWidth(b byte) PulseWave {
	return (PulseWave)((b >> 6) & 0x3)
}

func parsePlaybackMode(b byte) PlaybackMode {
	return (PlaybackMode)(b & 0x3)
}

func parseKitDistortion(b byte) KitDistortion {
	return KitDistortion(b)
}

func parseScommand(b byte) SCommand {
	return (SCommand)(b & 1)
}
