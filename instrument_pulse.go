package liblsdj

import "io"

type PulseWave byte

const (
	PulseWave125 PulseWave = iota
	PulseWave25
	PulseWave50
	PulseWave75
)

type InstrumentPulse struct {
	PulseWidth PulseWave
	Length     byte // 0x40 and above = unlimited
	Sweep      byte

	PlVibSpeed   PlVibSpeed
	VibShape     VibShape
	VibDirection VibDirection

	Transpose  byte
	DrumMode   byte
	Pulse2tune byte
	FineTune   byte
}

func (i *InstrumentPulse) Clear() {
	i.PulseWidth = PulseWave125
	i.Length = InstrumentUnlimitedLength
	i.Sweep = 0xFF

	i.PlVibSpeed = PlVibFast
	i.VibShape = VibTriangle
	i.VibDirection = VibUp

	i.Transpose = 1
	i.DrumMode = 0
	i.Pulse2tune = 0
	i.FineTune = 0
}

func (i *InstrumentPulse) Read(in *Instrument, r io.ReadSeeker, version byte) {
	var b byte
	i.Pulse2tune, _ = readByte(r)

	b, _ = readByte(r)
	i.Length = parseLength(b)

	i.Sweep, _ = readByte(r)

	b, _ = readByte(r)
	i.DrumMode = parseDrumMode(b, version)
	i.Transpose = parseTranspose(b, version)
	i.VibDirection = VibDirection(b & 1)

	in.Automate = parseAutomate(b)

	if version < 4 {
		switch (b >> 1) & 3 {
		case 0:
			i.PlVibSpeed = PlVibFast
			i.VibShape = VibTriangle
		case 1:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibSawtooth
		case 2:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibTriangle
		case 3:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibSquare
		}
	} else {
		if b&0x80 != 0 {
			i.PlVibSpeed = PlVibStep
		} else if b&0x10 != 0 {
			i.PlVibSpeed = PlVibTick
		} else {
			i.PlVibSpeed = PlVibFast
		}

		switch (b >> 1) & 3 {
		case 0:
			i.VibShape = VibTriangle
		case 1:
			i.VibShape = VibSawtooth
		case 2:
			i.VibShape = VibSquare
		}
	}
	b, _ = readByte(r)
	in.Table = parseTable(b)

	b, _ = readByte(r)
	in.Panning = parsePanning(b)
	i.PulseWidth = parsePulseWidth(b)
	i.FineTune = (b >> 2) & 0xf
	_, _ = r.Seek(8, io.SeekCurrent)
}

func (i *InstrumentPulse) Write(in *Instrument, w io.WriteSeeker, version byte) {
	var b byte

	_ = writeByte(0, w)
	_ = writeByte(in.EnvelopeVolume, w)
	_ = writeByte(i.Pulse2tune, w)

	_ = writeByte(createLengthByte(i.Length), w)
	_ = writeByte(i.Sweep, w)

	b = createDrumModeByte(i.DrumMode, version)
	b |= createTransposeByte(i.Transpose, version)
	b |= createAutomateByte(in.Automate)
	b |= createVibratoDirectionByte(i.VibDirection)

	if version < 4 {
		switch i.VibShape {
		case VibSawtooth:
			b |= 2
		case VibSquare:
			b |= 6
		case VibTriangle:
			if i.PlVibSpeed != PlVibFast {
				b |= 4
			}
		}
	} else {
		b |= (byte(i.VibShape) & 3) << 1
		if i.PlVibSpeed == PlVibTick {
			b |= 0x10
		} else if i.PlVibSpeed == PlVibStep {
			b |= 0x80
		}
	}

	_ = writeByte(b, w)
	_ = writeByte(createTableByte(in.Table), w)

	b = createPulseWidthByte(i.PulseWidth) | ((byte(i.FineTune) & 0xf) << 2) | createPanningByte(in.Panning)
	_ = writeByte(b, w)

	empty := []byte{0, 0, 0xD0, 0, 0, 0, 0xF3, 0}
	_, _ = w.Write(empty)
}
