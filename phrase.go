package liblsdj

const PhraseLen = 16

type Phrase struct {
	Notes       [PhraseLen]byte
	Instruments [PhraseLen]byte
	Commands    [PhraseLen]Command
}

func (p *Phrase) Clear() {
	for i := 0; i < PhraseLen; i++ {
		p.Notes[i] = 0
		p.Instruments[i] = 0xFF
		p.Commands[i].Clear()
	}
}
