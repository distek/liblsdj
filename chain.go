package liblsdj

const ChainLen = 16

type Chain struct {
	Phrases        [ChainLen]byte
	Transpositions [ChainLen]byte
}

func (c *Chain) Clear() {
	for i := 0; i < ChainLen; i++ {
		c.Phrases[i] = 0xFF
		c.Transpositions[i] = 0
	}
}
