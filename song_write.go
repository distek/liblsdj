package liblsdj

import "io"

func (s *Song) WriteBank0(w io.WriteSeeker) {
	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			_, _ = w.Write(s.Phrases[i].Notes[:])
		} else {
			_, _ = w.Write(PhraseLenZero[:])
		}
	}

	_, _ = w.Write(s.Bookmarks.Pulse1[:])
	_, _ = w.Write(s.Bookmarks.Pulse2[:])
	_, _ = w.Write(s.Bookmarks.Wave[:])
	_, _ = w.Write(s.Bookmarks.Noise[:])

	_, _ = w.Write(s.Reserved1030[:])

	for i := 0; i < len(s.Grooves); i++ {
		_, _ = w.Write(s.Grooves[i][:])
	}

	for i := 0; i < len(s.Rows); i++ {
		s.Rows[i].Write(w)
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			_, _ = w.Write(s.Tables[i].Volumes[:])
		} else {
			_, _ = w.Write(TableLengthZero[:])
		}
	}

	for i := 0; i < WordCnt; i++ {
		s.Words[i].Write(w)
	}

	for i := 0; i < WordCnt; i++ {
		_, _ = w.Write(s.WordNames[i][:])
	}

	_, _ = w.Write([]byte("rb"))

	for i := 0; i < InstrumentNameLen; i++ {
		if s.Instruments[i] != nil {
			_, _ = w.Write(s.Instruments[i].Name[:])
		} else {
			_, _ = w.Write(instrumentNameEmpty[:])
		}
	}

	_, _ = w.Write(s.Reserved1fba[:])
}

func (s *Song) WriteBank1(w io.WriteSeeker) {
	var instrAllocTable [InstrAllocTableSize]byte
	for i := 0; i < InstrCnt; i++ {
		if s.Instruments[i] != nil {
			instrAllocTable[i] = 1
		}
	}

	var tableAllocTable [TableAllocTableSize]byte
	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			tableAllocTable[i] = 1
		}
	}

	var chainAllocTable [ChainAllocTableSize]byte
	for i := 0; i < ChainCnt; i++ {
		if s.Chains[i] != nil {
			chainAllocTable[i/8] |= 1 << uint(i%8)
		}
	}

	var phraseAllocTable [PhraseAllocTableSize]byte
	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			phraseAllocTable[i/8] |= 1 << uint(i%8)
		}
	}

	_, _ = w.Write(s.Reserved2000[:])
	_, _ = w.Write(tableAllocTable[:])
	_, _ = w.Write(instrAllocTable[:])

	for i := 0; i < ChainCnt; i++ {
		if s.Chains[i] != nil {
			_, _ = w.Write(s.Chains[i].Phrases[:])
		} else {
			_, _ = w.Write(ChainLenFF[:])
		}
	}

	for i := 0; i < ChainCnt; i++ {
		if s.Chains[i] != nil {
			_, _ = w.Write(s.Chains[i].Transpositions[:])
		} else {
			_, _ = w.Write(ChainLenZero[:])
		}
	}

	for i := 0; i < InstrCnt; i++ {
		if s.Instruments[i] != nil {
			s.Instruments[i].Instrument.Write(s.Instruments[i], w, s.FormatVersion)
		} else {
			_, _ = w.Write(instrumentDefault[:])
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			_, _ = w.Write(s.Tables[i].Transpositions[:])
		} else {
			_, _ = w.Write(TableLengthZero[:])
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				_ = writeByte(s.Tables[i].Commands1[j].Command, w)
			}

		} else {
			_, _ = w.Write(TableLengthZero[:])
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				_ = writeByte(s.Tables[i].Commands1[j].Value, w)
			}

		} else {
			_, _ = w.Write(TableLengthZero[:])
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				_ = writeByte(s.Tables[i].Commands2[j].Command, w)
			}

		} else {
			_, _ = w.Write(TableLengthZero[:])
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				_ = writeByte(s.Tables[i].Commands2[j].Value, w)
			}

		} else {
			_, _ = w.Write(TableLengthZero[:])
		}
	}

	_, _ = w.Write([]byte("rb"))
	_, _ = w.Write(phraseAllocTable[:])
	_, _ = w.Write(chainAllocTable[:])

	for i := 0; i < SynthCnt; i++ {
		s.Synths[i].WriteSoftSynthParams(w)
	}

	_ = writeByte(s.Meta.WorkTime.Hours, w)
	_ = writeByte(s.Meta.WorkTime.Minutes, w)

	_ = writeByte(s.Tempo, w)
	_ = writeByte(s.Transposition, w)

	_ = writeByte(s.Meta.TotalTime.Days, w)
	_ = writeByte(s.Meta.TotalTime.Hours, w)
	_ = writeByte(s.Meta.TotalTime.Minutes, w)

	_ = writeByte(s.Reserved3fb9, w)
	_ = writeByte(s.Meta.KeyDelay, w)
	_ = writeByte(s.Meta.KeyRepeat, w)
	_ = writeByte(s.Meta.Font, w)
	_ = writeByte(s.Meta.Sync, w)
	_ = writeByte(s.Meta.ColorSet, w)
	_ = writeByte(s.Reserved3fbf, w)
	_ = writeByte(s.Meta.Clone, w)
	_ = writeByte(s.Meta.FileChangedFlag, w)
	_ = writeByte(s.Meta.PowerSave, w)
	_ = writeByte(s.Meta.PreListen, w)

	var waveSynthOverwriteLocks [2]byte
	for i := 0; i < SynthCnt; i++ {
		if s.Synths[i].Overwritten != 0 {
			waveSynthOverwriteLocks[1-(i/8)] |= 1 << uint(i%8)
		}
	}
	_, _ = w.Write(waveSynthOverwriteLocks[:])

	_, _ = w.Write(s.Reserved3fc6[:])
	_ = writeByte(s.DrumMax, w)
	_, _ = w.Write(s.Reserved3fd1[:])
}

func (s *Song) WriteBank2(w io.WriteSeeker) {
	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			for j := 0; j < PhraseLen; j++ {
				_ = writeByte(s.Phrases[i].Commands[j].Command, w)
			}
		} else {
			_, _ = w.Write(PhraseLenZero[:])
		}
	}

	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			for j := 0; j < PhraseLen; j++ {
				_ = writeByte(s.Phrases[i].Commands[j].Value, w)
			}
		} else {
			_, _ = w.Write(PhraseLenZero[:])
		}
	}

	_, _ = w.Write(s.Reserved5fe0[:])
}

func (s *Song) WriteBank3(w io.WriteSeeker) {
	for i := 0; i < WaveCnt; i++ {
		_, _ = w.Write(s.Waves[i][:])
	}

	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			_, _ = w.Write(s.Phrases[i].Instruments[:])
		} else {
			_, _ = w.Write(PhraseLenFF[:])
		}
	}

	_, _ = w.Write([]byte("rb"))

	_, _ = w.Write(s.Reserved7ff2[:])
	_ = writeByte(s.FormatVersion, w)
}

func WriteSong(w io.WriteSeeker, s *Song) {
	s.WriteBank0(w)
	s.WriteBank1(w)
	s.WriteBank2(w)
	s.WriteBank3(w)
}
