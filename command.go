package liblsdj

import "io"

const (
	CommandNone        = 0x00
	CommandA           = 0x01
	CommandB           = 0x17
	CommandC           = 0x02
	CommandD           = 0x03
	CommandE           = 0x04
	CommandF           = 0x05
	CommandG           = 0x06
	CommandH           = 0x07
	CommandK           = 0x08
	CommandL           = 0x09
	CommandM           = 0x0a
	CommandO           = 0x0b
	CommandP           = 0x0c
	CommandR           = 0x0d
	CommandS           = 0x0e
	CommandT           = 0x0f
	CommandV           = 0x10
	CommandW           = 0x11
	CommandZ           = 0x12
	CommandArduinoboyN = 0x13
	CommandArduinoboyX = 0x14
	CommandArduinoboyQ = 0x15
	CommandArduinoboyY = 0x16
)

type Command struct {
	Command byte
	Value   byte
}

func (c *Command) Clear() {
	c.Command = 0
	c.Value = 0
}

func (c *Command) Write(r io.ReadSeeker) {
	c.Command, _ = readByte(r)
	c.Value, _ = readByte(r)
}

func (c *Command) Get() []byte {
	var out [2]byte
	out[0] = c.Command
	out[1] = c.Value

	return out[:]
}
