package liblsdj

import (
	"io"
)

type PlVibSpeed byte
type VibShape byte
type VibDirection byte

const (
	InstrumentNameLen    = 5
	InstrumentDefaultLen = 16

	NoTable                   = 0x20
	InstrumentUnlimitedLength = 0x40
	KitLengthAuto             = 0x0

	InstrPulse byte = iota
	InstrWave
	InstrKit
	InstrNoise

	PlVibFast PlVibSpeed = iota
	PlVibTick
	PlVibStep

	VibTriangle VibShape = iota
	VibSawtooth
	VibSquare

	VibUp VibDirection = iota
	VibDown
)

type InstrumentT interface {
	Read(in *Instrument, r io.ReadSeeker, version byte)
	Write(in *Instrument, w io.WriteSeeker, version byte)
	Clear()
}

var instrumentDefault = [InstrumentDefaultLen]byte{0, 0xA8, 0, 0, 0xFF, 0, 0, 3, 0, 0, 0xD0, 0, 0, 0, 0xF3, 0}
var instrumentNameEmpty = [InstrumentNameLen]byte{0, 0, 0, 0, 0}

type Instrument struct {
	Name           [InstrumentNameLen]byte
	InsType        byte
	EnvelopeVolume byte
	Panning        Panning
	Table          byte // 0x20 or higher = LSDJ_NO_TABLE
	Automate       byte
	Instrument     InstrumentT
}

func (i *Instrument) ClearAsPulse() {
	i.InsType = InstrPulse
	i.EnvelopeVolume = 0xA8
	i.Panning = PanLeftRight
	i.Table = NoTable
	i.Automate = 0
	i.Instrument = new(InstrumentPulse)
	i.Instrument.Clear()
}

func (i *Instrument) Read(r io.ReadSeeker, version byte) {
	i.InsType, _ = readByte(r)

	pos1, _ := r.Seek(0, io.SeekCurrent)

	i.EnvelopeVolume, _ = readByte(r)

	switch i.InsType {
	case 0:
		i.Instrument = new(InstrumentPulse)
	case 1:
		i.Instrument = new(InstrumentWave)
	case 2:
		i.Instrument = new(InstrumentKit)
	case 3:
		i.Instrument = new(InstrumentNoise)
	default:
		panic("Strumento non conosciuto")
	}

	i.Instrument.Read(i, r, version)

	pos2, _ := r.Seek(0, io.SeekCurrent)

	// TODO ritorna errore per ste cagate
	if pos2-pos1 != 15 {
		panic("Non è 15")
	}

}
