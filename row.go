package liblsdj

import "io"

const ChannelCnt = 4

type Row struct {
	Pulse1 byte
	Pulse2 byte
	Wave   byte
	Noise  byte
}

func (ro *Row) Clear() {
	ro.Pulse1 = 0xFF
	ro.Pulse2 = 0xFF
	ro.Wave = 0xFF
	ro.Noise = 0xFF
}

func (ro *Row) Write(w io.WriteSeeker) {
	_ = writeByte(ro.Pulse1, w)
	_ = writeByte(ro.Pulse2, w)
	_ = writeByte(ro.Wave, w)
	_ = writeByte(ro.Noise, w)
}

func (ro *Row) Read(r io.ReadSeeker) {
	ro.Pulse1, _ = readByte(r)
	ro.Pulse2, _ = readByte(r)
	ro.Wave, _ = readByte(r)
	ro.Noise, _ = readByte(r)
}
