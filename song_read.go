package liblsdj

import (
	"fmt"
	"io"
)

func (s *Song) ReadBank0(r io.ReadSeeker) {
	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			if _, err := io.ReadFull(r, s.Phrases[i].Notes[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(PhraseLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	if _, err := io.ReadFull(r, s.Bookmarks.Pulse1[:]); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, s.Bookmarks.Pulse2[:]); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, s.Bookmarks.Wave[:]); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, s.Bookmarks.Noise[:]); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, s.Reserved1030[:]); err != nil {
		panic(err)
	}
	for i := 0; i < GrooveCnt; i++ {
		if _, err := io.ReadFull(r, s.Grooves[i][:]); err != nil {
			panic(err)
		}
	}
	for i := 0; i < RowCnt; i++ {
		s.Rows[i].Read(r)
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			if _, err := io.ReadFull(r, s.Tables[i].Volumes[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(TableLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < WordCnt; i++ {
		if _, err := io.ReadFull(r, s.Words[i].Allophones[:]); err != nil {
			panic(err)
		}
		if _, err := io.ReadFull(r, s.Words[i].Lenghts[:]); err != nil {
			panic(err)
		}
	}
	for i := 0; i < WordCnt; i++ {
		if _, err := io.ReadFull(r, s.WordNames[i][:]); err != nil {
			panic(err)
		}
	}

	//RB
	if _, err := r.Seek(2, io.SeekCurrent); err != nil {
		panic(err)
	}

	for i := 0; i < InstrCnt; i++ {
		if s.Instruments[i] != nil {
			if _, err := io.ReadFull(r, s.Instruments[i].Name[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(InstrumentNameLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	if _, err := io.ReadFull(r, s.Reserved1fba[:]); err != nil {
		panic(err)
	}
}

func (s *Song) ReadBank1(r io.ReadSeeker, version byte) {
	if _, err := io.ReadFull(r, s.Reserved2000[:]); err != nil {
		panic(err)
	}

	if _, err := r.Seek(TableAllocTableSize+InstrAllocTableSize, io.SeekCurrent); err != nil {
		panic(err)
	}

	for i := 0; i < ChainCnt; i++ {
		if s.Chains[i] != nil {
			if _, err := io.ReadFull(r, s.Chains[i].Phrases[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(ChainLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < ChainCnt; i++ {
		if s.Chains[i] != nil {
			if _, err := io.ReadFull(r, s.Chains[i].Transpositions[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(ChainLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < InstrCnt; i++ {
		if s.Instruments[i] != nil {
			s.Instruments[i].Read(r, version)
		} else {
			if _, err := r.Seek(16, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			if _, err := io.ReadFull(r, s.Tables[i].Transpositions[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(TableLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	// Twice command1
	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				s.Tables[i].Commands1[j].Command, _ = readByte(r)
			}
		} else {
			if _, err := r.Seek(TableLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				s.Tables[i].Commands1[j].Command, _ = readByte(r)
			}
		} else {
			if _, err := r.Seek(TableLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	// Twice command2
	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				s.Tables[i].Commands2[j].Command, _ = readByte(r)
			}
		} else {
			if _, err := r.Seek(TableLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < TableCnt; i++ {
		if s.Tables[i] != nil {
			for j := 0; j < TableLen; j++ {
				s.Tables[i].Commands2[j].Command, _ = readByte(r)
			}
		} else {
			if _, err := r.Seek(TableLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	//RB
	if _, err := r.Seek(2, io.SeekCurrent); err != nil {
		panic(err)
	}
	if _, err := r.Seek(PhraseAllocTableSize+ChainAllocTableSize, io.SeekCurrent); err != nil {
		panic(err)
	}

	for i := 0; i < SynthCnt; i++ {
		s.Synths[i].ReadSoftSynthParams(r)
	}

	s.Meta.WorkTime.Hours, _ = readByte(r)
	s.Meta.WorkTime.Minutes, _ = readByte(r)
	s.Tempo, _ = readByte(r)
	s.Transposition, _ = readByte(r)
	s.Meta.TotalTime.Days, _ = readByte(r)
	s.Meta.TotalTime.Hours, _ = readByte(r)
	s.Meta.TotalTime.Minutes, _ = readByte(r)
	s.Reserved3fb9, _ = readByte(r)
	s.Meta.KeyDelay, _ = readByte(r)
	s.Meta.KeyRepeat, _ = readByte(r)
	s.Meta.Font, _ = readByte(r)
	s.Meta.Sync, _ = readByte(r)
	s.Meta.ColorSet, _ = readByte(r)
	s.Reserved3fbf, _ = readByte(r)
	s.Meta.Clone, _ = readByte(r)
	s.Meta.FileChangedFlag, _ = readByte(r)
	s.Meta.PowerSave, _ = readByte(r)
	s.Meta.PreListen, _ = readByte(r)

	var waveSynthOverwriteLocks [2]byte
	if _, err := io.ReadFull(r, waveSynthOverwriteLocks[:]); err != nil {
		panic(err)
	}

	for i := 0; i < SynthCnt; i++ {
		s.Synths[i].Overwritten = ((waveSynthOverwriteLocks[1-(i/8)]) >> uint(i%8)) & 1
	}

	if _, err := io.ReadFull(r, s.Reserved3fc6[:]); err != nil {
		panic(err)
	}

	s.DrumMax, _ = readByte(r)

	if _, err := io.ReadFull(r, s.Reserved3fd1[:]); err != nil {
		panic(err)
	}
}

func (s *Song) ReadBank2(r io.ReadSeeker) {
	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			for j := 0; j < PhraseLen; j++ {
				s.Phrases[i].Commands[j].Command, _ = readByte(r)
			}
		} else {
			if _, err := r.Seek(PhraseLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}
	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			for j := 0; j < PhraseLen; j++ {
				s.Phrases[i].Commands[j].Value, _ = readByte(r)
			}
		} else {
			if _, err := r.Seek(PhraseLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	if _, err := io.ReadFull(r, s.Reserved5fe0[:]); err != nil {
		panic(err)
	}
}

func (s *Song) ReadBank3(r io.ReadSeeker) {
	for i := 0; i < WaveCnt; i++ {
		if _, err := io.ReadFull(r, s.Waves[i][:]); err != nil {
			panic(err)
		}
	}

	for i := 0; i < PhraseCnt; i++ {
		if s.Phrases[i] != nil {
			if _, err := io.ReadFull(r, s.Phrases[i].Instruments[:]); err != nil {
				panic(err)
			}
		} else {
			if _, err := r.Seek(PhraseLen, io.SeekCurrent); err != nil {
				panic(err)
			}
		}
	}

	//rb
	if _, err := r.Seek(2, io.SeekCurrent); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, s.Reserved7ff2[:]); err != nil {
		panic(err)
	}
	// Already read the version number
	if _, err := r.Seek(1, io.SeekCurrent); err != nil {
		panic(err)
	}
}

func CheckRb(r io.ReadSeeker, position int64) bool {
	var rb [2]byte
	if _, err := r.Seek(position, io.SeekStart); err != nil {
		panic(err)
	}

	if _, err := io.ReadFull(r, rb[:]); err != nil {
		panic(err)
	}

	return rb[0] == 'r' && rb[1] == 'b'
}

func ReadSong(r io.ReadSeeker, version byte) (s *Song, err error) {
	var tableAllocTable [TableAllocTableSize]byte
	var instrAllocTable [InstrAllocTableSize]byte
	var phraseAllocTable [PhraseAllocTableSize]byte
	var chainAllocTable [ChainAllocTableSize]byte

	s = new(Song)
	s.Clear()

	pos, _ := r.Seek(0, io.SeekCurrent)

	if !CheckRb(r, pos+0x1E78) {
		return s, fmt.Errorf("memory flag 'rb' not found at 0x1E78")
	}
	if !CheckRb(r, pos+0x3E80) {
		return s, fmt.Errorf("memory flag 'rb' not found at 0x3E80")
	}
	if !CheckRb(r, pos+0x7FF0) {
		return s, fmt.Errorf("memory flag 'rb' not found at 0x7FF0")
	}

	if _, err := r.Seek(pos+int64(0x7FFF), io.SeekStart); err != nil {
		panic(err)
	}

	s.FormatVersion, _ = readByte(r)

	if _, err := r.Seek(pos+int64(0x2020), io.SeekStart); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, tableAllocTable[:]); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, instrAllocTable[:]); err != nil {
		panic(err)
	}
	if _, err := r.Seek(pos+int64(0x3E82), io.SeekStart); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, phraseAllocTable[:]); err != nil {
		panic(err)
	}
	if _, err := io.ReadFull(r, chainAllocTable[:]); err != nil {
		panic(err)
	}
	/*
		Qui ragiono in logica opposta: se non c'è tabella pongo quell'indice a nil
	*/
	for i := 0; i < TableAllocTableSize; i++ {
		if tableAllocTable[i] == 0 {
			s.Tables[i] = nil
		} else {
			s.Tables[i] = new(Table)
			s.Tables[i].Clear()
		}
	}

	for i := 0; i < InstrAllocTableSize; i++ {
		if instrAllocTable[i] == 0 {
			s.Instruments[i] = nil
		} else {
			s.Instruments[i] = new(Instrument)
			s.Instruments[i].ClearAsPulse()
		}
	}

	for i := 0; i < 128; i++ {
		if (chainAllocTable[i/8]>>uint(i%8))&1 == 0 {
			s.Chains[i] = nil
		} else {
			// fmt.Printf("%02x\n", i)
			s.Chains[i] = new(Chain)
			s.Chains[i].Clear()
		}
	}

	for i := 0; i < 255; i++ {
		if (phraseAllocTable[i/8]>>uint(i%8))&1 == 0 {
			s.Phrases[i] = nil
		} else {
			s.Phrases[i] = new(Phrase)
			s.Phrases[i].Clear()
		}
	}

	if _, err := r.Seek(pos, io.SeekStart); err != nil {
		panic(err)
	}

	s.ReadBank0(r)
	s.ReadBank1(r, version)
	s.ReadBank2(r)
	s.ReadBank3(r)

	return
}
