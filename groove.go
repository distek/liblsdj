package liblsdj

const GrooveLen = 16

type Groove [GrooveLen]byte

var DefaultGroove = Groove{0x06, 0x06, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

func (g *Groove) Clear() {
	*g = DefaultGroove
}
