package liblsdj

import "io"

type PlaybackMode byte

const (
	PlayOnce PlaybackMode = iota
	PlayLoop
	PlayPingPong
	PlayManual
)

type InstrumentWave struct {
	PlVibSpeed   PlVibSpeed
	VibShape     VibShape
	VibDirection VibDirection

	Transpose byte
	DrumMode  byte
	Synth     byte
	Playback  PlaybackMode
	Length    byte
	Repeat    byte
	Speed     byte
}

func (i *InstrumentWave) Clear() {
	i.PlVibSpeed = PlVibFast
	i.VibShape = VibTriangle
	i.VibDirection = VibUp

	i.Transpose = 1
	i.DrumMode = 0
	i.Synth = 0
	i.Playback = PlayOnce
	i.Length = 0x0F
	i.Repeat = 0
	i.Speed = 4
}

func (i *InstrumentWave) Read(in *Instrument, r io.ReadSeeker, version byte) {
	var b byte

	b, _ = readByte(r)
	i.Synth = (b >> 4) & 0xf
	i.Repeat = b & 0xf

	// Byte 3 e 4 sono vuoti
	_, _ = r.Seek(2, io.SeekCurrent)

	b, _ = readByte(r)
	i.DrumMode = parseDrumMode(b, version)
	i.Transpose = parseTranspose(b, version)

	in.Automate = parseAutomate(b)
	if version < 4 {
		switch (b >> 1) & 3 {
		case 0:
			i.PlVibSpeed = PlVibFast
			i.VibShape = VibTriangle
		case 1:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibSawtooth
		case 2:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibTriangle
		case 3:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibSquare
		}
	} else {
		if b&0x80 != 0 {
			i.PlVibSpeed = PlVibStep
		} else if b&0x10 != 0 {
			i.PlVibSpeed = PlVibTick
		} else {
			i.PlVibSpeed = PlVibFast
		}

		switch (b >> 1) & 3 {
		case 0:
			i.VibShape = VibTriangle
		case 1:
			i.VibShape = VibSawtooth
		case 2:
			i.VibShape = VibSquare
		}
	}
	b, _ = readByte(r)
	in.Table = parseTable(b)

	b, _ = readByte(r)
	in.Panning = parsePanning(b)

	// byte 8 è vuoto
	_, _ = r.Seek(1, io.SeekCurrent)

	b, _ = readByte(r)
	i.Playback = parsePlaybackMode(b)

	// WAVE length and speed changed in version 6
	if version >= 7 {
		b, _ = readByte(r) // Byte 10
		i.Length = 0xf - (b & 0xf)

		b, _ = readByte(r) // Byte 11
		i.Speed = b + 4
	} else if version == 6 {
		b, _ = readByte(r) // Byte 10
		i.Length = b & 0xf

		b, _ = readByte(r) // Byte 11
		i.Speed = b + 1
	} else {
		_, _ = r.Seek(2, io.SeekCurrent) // Bytes 12-13 are empty
	}

	_, _ = r.Seek(2, io.SeekCurrent) // Bytes 10-13 are empty
	b, _ = readByte(r)
	if version < 6 {
		i.Length = (b >> 4) & 0xf
		i.Speed = (b & 0xf) + 1
	}

	_, _ = r.Seek(1, io.SeekCurrent) // Byte 15 is empty
}

func (i *InstrumentWave) Write(in *Instrument, w io.WriteSeeker, version byte) {
	var b byte

	// byte 0
	_ = writeByte(1, w)
	// byte 1
	_ = writeByte(createWaveVolumeByte(in.EnvelopeVolume), w)
	// byte 2
	b = ((i.Synth & 0xf) << 4) | (i.Repeat & 0xf)
	_ = writeByte(b, w)
	// byte 3 empty
	_ = writeByte(0, w)
	// byte 4 empty
	_ = writeByte(0xff, w)

	b = createDrumModeByte(i.DrumMode, version)
	b |= createTransposeByte(i.Transpose, version)
	b |= createAutomateByte(in.Automate)
	b |= createVibratoDirectionByte(i.VibDirection)
	if version < 4 {
		switch i.VibShape {
		case VibSawtooth:
			b |= 2
		case VibSquare:
			b |= 6
		case VibTriangle:
			if i.PlVibSpeed != PlVibFast {
				b |= 4
			}
		}
	} else {
		b |= (byte(i.VibShape) & 3) << 1
		if i.PlVibSpeed == PlVibTick {
			b |= 0x10
		} else if i.PlVibSpeed == PlVibStep {
			b |= 0x80
		}
	}
	_ = writeByte(b, w)
	_ = writeByte(createTableByte(in.Table), w)
	_ = writeByte(createPanningByte(in.Panning), w)
	_ = writeByte(0, w)

	_ = writeByte(createPlaybackModeByte(i.Playback), w)

	if version >= 7 {
		b = 0xf - (i.Length & 0xF)
		_ = writeByte(b, w)

		b = i.Speed - 4
		_ = writeByte(b, w)
	} else if version == 6 {
		b = i.Length & 0xF
		_ = writeByte(b, w)

		b = i.Speed - 1
		_ = writeByte(b, w)
	} else {
		_ = writeByte(0, w) // byte 10 is empty
		_ = writeByte(0, w) // byte 11 is empty
	}

	_ = writeByte(0, w) // byte 12 is empty
	_ = writeByte(0, w) // byte 13 is empty

	if version < 6 {
		b = ((i.Length & 0xf) << 4) | ((i.Speed - 1) & 0xf)
		_ = writeByte(b, w)
	} else {
		_ = writeByte(0, w) // byte 14 is empty
	}

	_ = writeByte(0, w) // byte 15 is empty
}
