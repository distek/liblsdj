package liblsdj

import "io"

type SCommand byte

const (
	SCommandFree SCommand = iota
	SCommandStable
)

type InstrumentNoise struct {
	Length   byte
	Shape    byte
	SCommand SCommand
}

func (i *InstrumentNoise) Clear() {
	i.Length = InstrumentUnlimitedLength
	i.Shape = 0xFF
	i.SCommand = SCommandFree
}

func (i *InstrumentNoise) Read(in *Instrument, r io.ReadSeeker, version byte) {
	var b byte

	b, _ = readByte(r)
	i.SCommand = parseScommand(b)

	b, _ = readByte(r)
	i.Length = parseLength(b)

	i.Shape, _ = readByte(r)

	b, _ = readByte(r)
	in.Automate = parseAutomate(b)

	b, _ = readByte(r)
	in.Table = parseTable(b)

	b, _ = readByte(r)
	in.Panning = parsePanning(b)

	_, _ = r.Seek(8, io.SeekCurrent) // Bytes 8-15 are empty
}

func (i *InstrumentNoise) Write(in *Instrument, w io.WriteSeeker, version byte) {

	_ = writeByte(3, w)
	_ = writeByte(in.EnvelopeVolume, w)
	_ = writeByte(createScommandByte(i.SCommand), w)
	_ = writeByte(createLengthByte(i.Length), w)
	_ = writeByte(i.Shape, w)
	_ = writeByte(createAutomateByte(in.Automate), w)
	_ = writeByte(createTableByte(in.Table), w)
	_ = writeByte(createPanningByte(in.Panning), w)

	empty := []byte{0, 0, 0xD0, 0, 0, 0, 0xF3, 0}
	_, _ = w.Write(empty) // Bytes 8-15 are empty
}
