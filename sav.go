package liblsdj

const (
	NoActiveProject = 0xff
	SavProjectCnt   = 32
	HeaderStart     = SongDecompSize
	BlockCnt        = 191
	BlockSize       = 0x200
)

type Sav struct {
	Projects     [SavProjectCnt]*Project
	Active       byte
	ActiveSong   *Song
	Reserved8120 [30]byte
}

type HeaderT struct {
	ProjectNames  [SavProjectCnt * 8]byte
	Versions      [SavProjectCnt * 1]byte
	Empty         [30]byte
	Init          [2]byte
	ActiveProject byte
}

func (s *Sav) SetWorkingSong(so *Song, activeProject byte) {
	s.ActiveSong = so
	s.Active = activeProject
}

func SavWrite(s *Sav) {
	var header HeaderT

	header.Init[0] = 'j'
	header.Init[1] = 'k'
	header.ActiveProject = s.Active

	//TODO empty and reservec

	// Create the block allocation table for writing
	var blockAllocTable [BlockCnt]byte
	//var blocks [blockCnt][blockSize]byte

	for i := 0; i < BlockCnt; i++ {
		blockAllocTable[i] = 0xff
	}

	for i := 0; i < SavProjectCnt; i++ {

	}

}
