package liblsdj

import "io"

const (
	SynthWaveformSawtooth = 0
	SynthWaveformSquare   = 1
	SynthWaveformTriangle = 2

	SynthFilterLowPass  = 0
	SynthFilterHighPass = 1
	SynthFilterBandPass = 2
	SynthFilterAllPass  = 3

	SynthDistortionClip = 0
	SynthDistortionWrap = 1
	SynthDistortionFold = 2

	SynthPhaseNormal  = 0
	SynthPhaseResync  = 1
	SynthPhaseResync2 = 2
)

type Synth struct {
	Waveform       byte
	Filter         byte
	ResonanceStart byte
	ResonanceEnd   byte
	Distortion     byte
	Phase          byte

	VolumeStart byte
	VolumeEnd   byte
	CutOffStart byte
	CutOffEnd   byte

	PhaseStart  byte
	PhaseEnd    byte
	VshiftStart byte
	VshiftEnd   byte

	LimitStart byte
	LimitEnd   byte

	Reserved [2]byte

	Overwritten byte // 0 if false, 1 if true
}

func (s *Synth) Clear() {
	s.Waveform = SynthWaveformSawtooth
	s.Filter = SynthFilterLowPass
	s.ResonanceStart = 0
	s.ResonanceEnd = 0
	s.Distortion = SynthDistortionClip
	s.Phase = SynthPhaseNormal
	s.VolumeStart = 0x10
	s.CutOffStart = 0xFF
	s.PhaseStart = 0
	s.VshiftStart = 0
	s.VolumeEnd = 0x10
	s.CutOffEnd = 0xFF
	s.PhaseEnd = 0
	s.VshiftEnd = 0
	s.LimitStart = 0xF
	s.LimitEnd = 0xF
	s.Reserved[0] = 0
	s.Reserved[1] = 0

	s.Overwritten = 0
}

func (s *Synth) ReadSoftSynthParams(r io.ReadSeeker) {
	s.Waveform, _ = readByte(r)
	s.Filter, _ = readByte(r)

	resonance, _ := readByte(r)
	s.ResonanceStart = (resonance & 0xF0) >> 4
	s.ResonanceEnd = resonance & 0x0F

	s.Distortion, _ = readByte(r)
	s.Phase, _ = readByte(r)
	s.VolumeStart, _ = readByte(r)
	s.CutOffStart, _ = readByte(r)
	s.PhaseStart, _ = readByte(r)
	s.VshiftStart, _ = readByte(r)
	s.VolumeEnd, _ = readByte(r)
	s.CutOffEnd, _ = readByte(r)
	s.PhaseEnd, _ = readByte(r)
	s.VshiftEnd, _ = readByte(r)

	b, _ := readByte(r)
	b -= 0xFF
	s.LimitStart = (b >> 4) & 0xF
	s.LimitEnd = b & 0xF

	s.Reserved[0], _ = readByte(r)
	s.Reserved[1], _ = readByte(r)
}

func (s *Synth) WriteSoftSynthParams(w io.WriteSeeker) {
	_ = writeByte(s.Waveform, w)
	_ = writeByte(s.Distortion, w)
	_ = writeByte(s.Phase, w)
	_ = writeByte(s.VolumeStart, w)
	_ = writeByte(s.CutOffStart, w)
	_ = writeByte(s.VshiftStart, w)
	_ = writeByte(s.VolumeEnd, w)
	_ = writeByte(s.CutOffEnd, w)
	_ = writeByte(s.PhaseEnd, w)
	_ = writeByte(s.VshiftEnd, w)

	b := 0xFF - (s.LimitStart << 4) | s.LimitEnd
	_ = writeByte(b, w)

	_, _ = w.Write(s.Reserved[:])
}
