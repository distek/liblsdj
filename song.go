package liblsdj

const (
	SongDecompSize = 0x8000
	RowCnt         = 256
	ChainCnt       = 128
	PhraseCnt      = 0xFF
	InstrCnt       = 64
	SynthCnt       = 16
	TableCnt       = 32
	WaveCnt        = 256
	GrooveCnt      = 32
	WordCnt        = 42
	BookmarkPosCnt = 16
	NoBookmark     = 0xFF
	CloneDeep      = 0
	CloneSlim      = 1

	InstrAllocTableSize  = 64
	TableAllocTableSize  = 32
	ChainAllocTableSize  = 16
	PhraseAllocTableSize = 32
)

var TableLengthZero = [TableLen]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
var ChainLenZero = [TableLen]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
var ChainLenFF = [TableLen]byte{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
var PhraseLenZero = [PhraseLen]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
var PhraseLenFF = [PhraseLen]byte{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}

var WordNameDefault = [WordCnt][WordNameLen]byte{{'C', ' ', '2', ' '}, {'C', ' ', '2', ' '}, {'D', ' ', '2', ' '}, {'D', ' ', '2', ' '}, {'E', ' ', '2', ' '}, {'F', ' ', '2', ' '}, {'F', ' ', '2', ' '}, {'G', ' ', '2', ' '}, {'G', ' ', '2', ' '}, {'A', ' ', '2', ' '}, {'A', ' ', '2', ' '}, {'B', ' ', '2', ' '}, {'C', ' ', '3', ' '}, {'C', ' ', '3', ' '}, {'D', ' ', '3', ' '}, {'D', ' ', '3', ' '}, {'E', ' ', '3', ' '}, {'F', ' ', '3', ' '}, {'F', ' ', '3', ' '}, {'G', ' ', '3', ' '}, {'G', ' ', '3', ' '}, {'A', ' ', '3', ' '}, {'A', ' ', '3', ' '}, {'B', ' ', '3', ' '}, {'C', ' ', '4', ' '}, {'C', ' ', '4', ' '}, {'D', ' ', '4', ' '}, {'D', ' ', '4', ' '}, {'E', ' ', '4', ' '}, {'F', ' ', '4', ' '}, {'F', ' ', '4', ' '}, {'G', ' ', '4', ' '}, {'G', ' ', '4', ' '}, {'A', ' ', '4', ' '}, {'A', ' ', '4', ' '}, {'B', ' ', '4', ' '}, {'C', ' ', '5', ' '}, {'C', ' ', '5', ' '}, {'D', ' ', '5', ' '}, {'D', ' ', '5', ' '}, {'E', ' ', '5', ' '}, {'F', ' ', '5', ' '}}

type Song struct {
	FormatVersion byte
	Tempo         byte
	Transposition byte
	DrumMax       byte

	Rows        [RowCnt]Row
	Chains      [ChainCnt]*Chain
	Phrases     [PhraseCnt]*Phrase
	Instruments [InstrCnt]*Instrument
	Synths      [SynthCnt]Synth
	Waves       [WaveCnt]Wave
	Tables      [TableCnt]*Table
	Grooves     [GrooveCnt]Groove

	Words     [WordCnt]Word
	WordNames [WordCnt][WordNameLen]byte

	Bookmarks struct {
		Pulse1 [BookmarkPosCnt]byte
		Pulse2 [BookmarkPosCnt]byte
		Wave   [BookmarkPosCnt]byte
		Noise  [BookmarkPosCnt]byte
	}

	//TODO: bookmarks
	Meta struct {
		KeyDelay        byte
		KeyRepeat       byte
		Font            byte
		Sync            byte
		ColorSet        byte
		Clone           byte
		FileChangedFlag byte
		PowerSave       byte
		PreListen       byte

		TotalTime struct {
			Days    byte
			Hours   byte
			Minutes byte
		}
		WorkTime struct {
			Hours   byte
			Minutes byte
		}
	}
	Reserved1030 [96]byte
	Reserved1fba [70]byte
	Reserved2000 [32]byte
	Reserved3fbf byte
	Reserved3fb9 byte
	Reserved3fc6 [10]byte
	Reserved3fd1 [47]byte
	Reserved5fe0 [32]byte
	Reserved7ff2 [13]byte
}

func (s *Song) Clear() {
	s.FormatVersion = 4
	s.Tempo = 128
	s.Transposition = 0
	s.DrumMax = 0x6C

	for i := 0; i < RowCnt; i++ {
		s.Rows[i].Clear()
	}
	for i := 0; i < WaveCnt; i++ {
		s.Waves[i].Clear()
	}
	for i := 0; i < GrooveCnt; i++ {
		s.Grooves[i].Clear()
	}
	for i := 0; i < ChainCnt; i++ {
		s.Chains[i] = nil
	}
	for i := 0; i < PhraseCnt; i++ {
		s.Phrases[i] = nil
	}
	for i := 0; i < InstrCnt; i++ {
		s.Instruments[i] = nil
	}
	for i := 0; i < SynthCnt; i++ {
		s.Synths[i].Clear()
	}

	for i := 0; i < TableCnt; i++ {
		s.Tables[i] = nil
	}
	for i := 0; i < WordCnt; i++ {
		s.Words[i].Clear()
	}
	s.WordNames = WordNameDefault

	/*
		memset(&Song->bookmarks, LSDJ_NO_BOOKMARK, sizeof(Song->bookmarks));
	*/

	s.Meta.KeyDelay = 7
	s.Meta.KeyRepeat = 2
	s.Meta.Font = 0
	s.Meta.Sync = 0
	s.Meta.ColorSet = 0
	s.Meta.Clone = 0
	s.Meta.FileChangedFlag = 0
	s.Meta.PowerSave = 0
	s.Meta.PreListen = 1

	s.Meta.TotalTime.Days = 0
	s.Meta.TotalTime.Hours = 0
	s.Meta.TotalTime.Minutes = 0
	s.Meta.WorkTime.Hours = 0
	s.Meta.WorkTime.Minutes = 0
}
