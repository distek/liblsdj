package liblsdj

import "io"

type KitLoopMode byte
type KitDistortion byte
type KitPspeed byte

const (
	KitDistClip   KitDistortion = 0xD0
	KitDistShape  KitDistortion = 0xD1
	KitDistShape2 KitDistortion = 0xD2
	KitDistWrap   KitDistortion = 0xD3

	KitLoopOff KitLoopMode = iota
	KitLoopOn
	KitLoopAttack

	KitPspeedFast KitPspeed = iota
	KitPspeedSlow
	KitPspeedStep
)

type InstrumentKit struct {
	Kit1    byte
	Offset1 byte
	Length1 byte
	Loop1   KitLoopMode

	Kit2    byte
	Offset2 byte
	Length2 byte
	Loop2   KitLoopMode

	Pitch        byte
	HalfSpeed    byte
	Distortion   KitDistortion
	PlVibSpeed   PlVibSpeed
	VibShape     VibShape
	VibDirection VibDirection
}

func (i *InstrumentKit) Clear() {
	i.Kit1 = 0
	i.Offset1 = 0
	i.Length1 = KitLengthAuto
	i.Loop1 = KitLoopOff

	i.Kit2 = 0
	i.Offset2 = 0
	i.Length2 = KitLengthAuto
	i.Loop2 = KitLoopOff

	i.Pitch = 0
	i.HalfSpeed = 0
	i.Distortion = KitDistClip
	i.PlVibSpeed = PlVibFast
	i.VibShape = VibTriangle
}

func (i *InstrumentKit) Read(in *Instrument, r io.ReadSeeker, version byte) {
	var b byte

	i.Loop1 = KitLoopOff
	i.Loop2 = KitLoopOff

	b, _ = readByte(r)
	// TODO controlla condizioni
	if (b>>7)&1 != 0 {
		i.Loop1 = KitLoopAttack
	}
	i.HalfSpeed = (b >> 6) & 1
	i.Kit1 = b & 0x3f
	i.Length1, _ = readByte(r)

	// Byte 4 vuoto
	_, _ = r.Seek(1, io.SeekCurrent)
	b, _ = readByte(r)

	if i.Loop1 != KitLoopAttack {
		if b&0x40 != 0 {
			i.Loop1 = KitLoopOn
		} else {
			i.Loop1 = KitLoopOff
		}
	}

	if b&0x20 != 0 {
		i.Loop2 = KitLoopOn
	} else {
		i.Loop2 = KitLoopOff
	}

	in.Automate = parseAutomate(b)
	i.VibDirection = VibDirection(b & 1)

	if version < 4 {
		switch (b >> 1) & 3 {
		case 0:
			i.PlVibSpeed = PlVibFast
			i.VibShape = VibTriangle
		case 1:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibSawtooth
		case 2:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibTriangle
		case 3:
			i.PlVibSpeed = PlVibTick
			i.VibShape = VibSquare
		}
	} else {
		if b&0x80 != 0 {
			i.PlVibSpeed = PlVibStep
		} else if b&0x10 != 0 {
			i.PlVibSpeed = PlVibTick
		} else {
			i.PlVibSpeed = PlVibFast
		}

		switch (b >> 1) & 3 {
		case 0:
			i.VibShape = VibTriangle
		case 1:
			i.VibShape = VibSawtooth
		case 2:
			i.VibShape = VibSquare
		}
	}

	// byte 6
	b, _ = readByte(r)
	in.Table = parseTable(b)

	// byte 7
	b, _ = readByte(r)
	in.Panning = parsePanning(b)

	// byte 8
	i.Pitch, _ = readByte(r)

	// byte 9
	b, _ = readByte(r)
	if (b>>7)&1 != 0 {
		i.Loop2 = KitLoopAttack
	}
	i.Kit2 = b & 0x3f

	// byte 10
	b, _ = readByte(r)
	i.Distortion = parseKitDistortion(b)
	// byte 11
	i.Length2, _ = readByte(r)
	// byte 12
	i.Offset1, _ = readByte(r)
	// byte 13
	i.Offset2, _ = readByte(r)

	_, _ = r.Seek(2, io.SeekCurrent) // Bytes 14 and 15 are empty
}

func (i *InstrumentKit) Write(in *Instrument, w io.WriteSeeker, version byte) {
	var b byte

	_ = writeByte(2, w)
	_ = writeByte(createWaveVolumeByte(in.EnvelopeVolume), w)

	// An advice: ternary operator sucks
	if i.Loop1 == KitLoopAttack {
		b = 0x80
	} else {
		b = 0x0
	}
	if i.HalfSpeed == 1 {
		b = b | 0x40
	} else {
		b = b | 0x0
	}
	b = b | (i.Kit1 & 0x3f)
	_ = writeByte(b, w)

	_ = writeByte(i.Length1, w)
	_ = writeByte(0xff, w)

	if i.Loop1 == KitLoopOn {
		b = 0x40
	} else {
		b = 0x0
	}

	// Byte 5
	if i.Loop2 == KitLoopOn {
		b = b | 0x40
	} else {
		b = b | 0x0
	}
	b = b | createAutomateByte(in.Automate)

	if version < 4 {
		b = b | (byte(i.PlVibSpeed)&3)<<1
	} else {
		switch i.PlVibSpeed {
		// plVibFast does nothing
		case PlVibTick:
			b = b | 0x10
		case PlVibStep:
			b = b | 0x80
		}
	}
	_ = writeByte(b, w)

	_ = writeByte(createTableByte(in.Table), w)
	_ = writeByte(createPanningByte(in.Panning), w)
	_ = writeByte(i.Pitch, w)

	if i.Loop2 == KitLoopAttack {
		b = 0x80
	} else {
		b = 0x0
	}
	b = b | (i.Kit2 & 0x3f)
	_ = writeByte(b, w)

	_ = writeByte(createKitDistortionByte(i.Distortion), w)
	_ = writeByte(i.Length2, w)
	_ = writeByte(i.Offset1, w)
	_ = writeByte(i.Offset2, w)
	_ = writeByte(0xf3, w) // Byte 14 is empty
	_ = writeByte(0, w)    // Byte 15 is empty
}
