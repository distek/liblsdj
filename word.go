package liblsdj

import "io"

const (
	WordLen     = 16
	WordNameLen = 4
)

type Word struct {
	Allophones [WordLen]byte
	Lenghts    [WordLen]byte
}

func (w *Word) Clear() {
	for i := 0; i < WordLen; i++ {
		w.Allophones[i] = 0
		w.Lenghts[i] = 0
	}
}

func (wo *Word) Write(w io.WriteSeeker) {
	_, _ = w.Write(wo.Allophones[:])
	_, _ = w.Write(wo.Lenghts[:])
}
