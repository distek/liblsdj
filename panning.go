package liblsdj

type Panning byte

const (
	PanNone Panning = iota
	PanRight
	PanLeft
	PanLeftRight
)
