package liblsdj

import (
	"fmt"
	"io"

	"github.com/orcaman/writerseeker"
)

const ProjectNameLen = 8

type Project struct {
	Name    [ProjectNameLen]byte
	Version byte
	Song    *Song
}

func ReadLsdsng(r io.ReadSeeker) (p Project) {
	_, _ = io.ReadFull(r, p.Name[:])
	p.Version, _ = readByte(r)

	b := new(writerseeker.WriterSeeker)
	decompress(r, b, nil)
	p.Song, _ = ReadSong(b.BytesReader(), p.Version)

	return p
}

func WriteLsdsng(w io.WriteSeeker, p Project) {
	// TODO controlla se c'è la canzona
	// if song == null
	// if name
	// if version

	_, _ = w.Write(p.Name[:])
	_ = writeByte(p.Version, w)

	tmp := make([]byte, SongDecompSize)
	for i := 0; i < SongDecompSize; i++ {
		tmp[i] = 0x34
	}
	b := new(writerseeker.WriterSeeker)
	_, _ = b.Write(tmp)
	_, _ = b.Seek(0, io.SeekStart)
	WriteSong(b, p.Song)
	fmt.Println(compress(b.BytesReader(), w, 1))
}
